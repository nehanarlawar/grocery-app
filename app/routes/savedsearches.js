import Ember from 'ember';
import ShoplistModel from 'newgrocery/models/shoplist';
import searchModel from 'newgrocery/models/search';
var ShopRoute = Ember.Route.extend({

    model: function() {

        var user_name=this.controllerFor("index").get('userName');
        var params={user_name : user_name, operation:"search"};
        return Ember.$.ajax({
            url: 'php_dataconnectivity_new.php',
            method: 'GET',
            dataType: 'json',
            data: params ? params : null
        }).then(function(result) {
            var searches = [];
            for(var i=0; i<result.length; i++) {
                searches.push(searchModel.create(result[i]));
            }
     return searches;

        });
    },

});


export default ShopRoute;




