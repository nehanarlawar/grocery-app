
import Ember from 'ember';


var DirectionMapController=Ember.Controller.extend({
   currLocLat:null,
   currLocLng:null,
   destinationAddress:null,

   init:function()
    {
        this._super();
        var shopcontroller=this.controllerFor('index');
        this.set('currLocLat', shopcontroller.get('latitude'));
        this.set('currLocLng', shopcontroller.get('longitude'));
        this.set('destinationAddress', shopcontroller.get('destinationAddress'));

    }


});

export default DirectionMapController;